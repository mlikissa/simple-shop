This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To run this on your local machine
### `git clone https://gitlab.com/mlikissa/simple-shop.git`
### `cd simple-shop`
### `yarn install`
### `yarn start`
