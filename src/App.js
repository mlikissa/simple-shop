import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import styled from 'styled-components';

//organism components
import Header from "organisms/Header"

//pages
import Product from 'pages/Product'
import Checkout from 'pages/Checkout'
import FourOhFour from 'pages/FourOhFour'

//context
import { CartProvider } from 'contexts/cart-context';


const App = ({className}) => (
    <Router>
      <div className={className}>
        <Header />
        <Switch>
          <CartProvider>
            <Route exact path="/" component={Product} />
            <Route path="/checkout" component={Checkout} />
          </CartProvider>
          <Route component={FourOhFour} />
        </Switch>

      </div>
    </Router>
  );

const StyledApp = styled(App)`
  font-family: Arial;
`
export default StyledApp;
