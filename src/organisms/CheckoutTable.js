import React from 'react';
import styled from 'styled-components';

//molecules
import TableBody from 'molecules/TableBody'
import TableHeader from 'molecules/TableHeader'

//context
import { CartConsumer } from 'contexts/cart-context';

const CheckoutTable = () => (
  <StyledTable>
    <tbody>
      <TableHeader />
      <CartConsumer>
        {({cart, addToCart,subtractCartItem, removeCartItem }) =>
          cart.Quantity?
            <TableBody
              cart={cart}
              addToCart={addToCart}
              subtractCartItem={subtractCartItem}
              removeCartItem={removeCartItem}
            />
          :<tr><td>Please select some items to purchase</td></tr>
        }
      </CartConsumer>
    </tbody>
  </StyledTable>
)

const StyledTable = styled.table`
  width: 100%;
  text-align: left;
  margin: 0 1.5em;
  border-collapse: collapse;

  @media(max-width: 768px) {
    width:auto;
  }
`
export default CheckoutTable
