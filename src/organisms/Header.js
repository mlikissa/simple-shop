import React from 'react';
import styled from 'styled-components';

//molecules
import NavBar from 'molecules/NavBar'
//atoms
import Logo from 'atoms/Logo'


const Header = ({className}) =>  (
    <div className={className}>
      <Logo />
      <NavBar />
    </div>
);

const StyledHeader = styled(Header)`
  display:flex;
  flex-direction:row;
  justify-content:space-between;
  align-items:center;
  padding: 0 1.5em;

  @media(max-width: 768px) {
    padding: 0;
  }
`;

export default StyledHeader;
