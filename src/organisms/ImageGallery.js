import React from 'react';
import ImageGallery from 'react-image-gallery';

//css
import "react-image-gallery/styles/css/image-gallery.css";

const Gallery = ({gallery}) =>(
    <ImageGallery items={gallery} />
)
export default Gallery;
