import React from 'react';
//atoms
import Button from 'atoms/Button'
//context
import { CartConsumer } from 'contexts/cart-context';

const ProductDetails = ({product}) =>(
    <div>
      <h1>{product.Title}</h1>
      <p>{product.Description}</p>
      <CartConsumer>
          {({ addToCart,cart }) =>
            <Button onClick={()=>addToCart(product)}primary>{cart.hasOwnProperty("ProductId")? cart.Quantity + " added to Cart":"Add to Cart £ " + product.Price}</Button>
          }
      </CartConsumer>
    </div>
)
export default ProductDetails;
