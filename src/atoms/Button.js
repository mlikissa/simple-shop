import styled from 'styled-components';

const Button = styled.button`
  color: ${props => props.warning ? "#721c24":"rgba(0, 0, 0, 0.54)"};
  font-size: 1em;
  margin: 1em;
  padding: ${props => props.small ? "0.25em 0.5em" : "0.25em 1em"};
  border: ${props => props.warning ? " 2px solid  #f5c6cb" : " 2px solid rgb(125, 255, 179);"};
  border-radius: 3px;
  cursor:pointer;
  background: ${props => props.warning ? "#f5c6cb" : "white"};
  :hover{
        background: ${props => props.warning ? "#f8d7da" : "background:rgb(125, 255, 179)"};
  }
  :disabled{
    border: 2px solid #eaeaea;
    :hover{
      background:white;
    }
  }
`;

export default Button;
