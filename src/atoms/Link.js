import React from 'react';
import styled from 'styled-components';
import { Link } from "react-router-dom";


const PlainLink = ({ className,href,title }) => (
  <Link className={className} to={href} >
    {title}
  </Link>
);

const StyledLink = styled(PlainLink)`
  color: rgba(0,0,0,.5);
  text-decoration:none;
  padding:0 1em;
`;


export default StyledLink;
