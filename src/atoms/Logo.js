import React from 'react';
import Image from 'assets/default-logo.png'

const Logo = () => (
    <img width='50' src={Image} alt="ACME Logo" />
  )
export default Logo;
