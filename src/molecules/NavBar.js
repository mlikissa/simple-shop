import React from 'react';
import StyledLink from 'atoms/Link'

const NavBar = () =>(
    <div>
    <StyledLink href={"/"} title={"Home"} />
    <StyledLink href={"/checkout"} title={"Cart"} />
    </div>
)
export default NavBar;
