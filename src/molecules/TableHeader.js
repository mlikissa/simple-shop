import React from 'react';

const TableHeader = () => (
    <tr>
      <th>Name</th>
      <th>Quantity</th>
      <th>Price</th>
      <th>Total</th>
    </tr>
  )
export default TableHeader;
