import React from 'react';
import styled from 'styled-components';

//components
import Button from 'atoms/Button';

const TableBody = ({cart,addToCart,subtractCartItem, removeCartItem}) => (
    <StyledTr>
      <td><Button warning onClick={()=>removeCartItem()}>remove</Button> {cart.Title} </td>
      <StyledTd><Button small onClick={()=>addToCart(cart)}>+</Button>{cart.Quantity}<Button small disabled={cart.Quantity===1} onClick={()=>subtractCartItem()}>-</Button></StyledTd>
      <td>£ {cart.Price}</td>
      <td>£ {(cart.Price * cart.Quantity).toFixed(2)}</td>
    </StyledTr>
  )

const StyledTd = styled.td`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 3em;
`
const StyledTr = styled.tr`
  border: 1px solid #aeaeae;
`
export default TableBody;
