import React, { createContext } from 'react';

const CartContext = createContext({
  cart:{},
  addToCart:()=>{},
  subtractCartItem:()=>{},
  removeCartItem:()=>{}
});

export class CartProvider extends React.Component {

  subtractCartItem = cartItem => {
    this.setState(prevState => {

      if(prevState.cart.Quantity > 1){
        let newCart = {...prevState.cart};
        newCart.Quantity += -1;
        return {cart: newCart};
      }
    });
  }
  removeCartItem = () => {
    this.setState({cart: {}});
  }

  addToCart = (selectedProduct) => {
    if(this.state.cart.hasOwnProperty("ProductId")){
      this.setState(prevState => {
        let newCart = {...prevState.cart};
        newCart.Quantity += 1;
        return {cart: newCart};
      });
    }else{
      this.setState({
        cart:{
            ProductId: selectedProduct.ProductId,
            Title: selectedProduct.Title,
            Price: selectedProduct.Price,
            Quantity: 1
        }
      })
    }

  }


  state = {
    cart:{},
    addToCart:this.addToCart,
    subtractCartItem:this.subtractCartItem,
    removeCartItem:this.removeCartItem
  };

  render() {
    return (
      <CartContext.Provider value={this.state} {...this.props}>

        {this.props.children}
      </CartContext.Provider>
    );
  }
}

export const CartConsumer = CartContext.Consumer;
