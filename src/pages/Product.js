import React from 'react';
import styled from 'styled-components';

//organisms
import ImageGallery from 'organisms/ImageGallery'
import ProductDetails from 'organisms/ProductDetails'


const demoProduct ={
         "Price":3.49,
         "Title":"Genric ACME Product",
         "ProductId":48939,
         "Description":"Quem quas perfecto id mei. Populo prompta imperdiet te per. Dicunt civibus pro ex. Autem paulo civibus nam no, laboramus reformidans et ius. Nostrud placerat percipit qui ei, vide persecuti ad sit. Eum eu movet timeam, sea tamquam adipisci expetendis an. Vel in temporibus contentiones. Ius eu esse nibh velit. Mel ex partem probatus, possim forensibus te eos, in nec facilisi mnesarchum. An nostro cotidieque usu. Omittam ancillae mel cu, ut mel constituam accommodare, ius ut democritum necessitatibus.",
         "ImageGallery":[
           {
             original: 'https://picsum.photos/id/1018/1000/600/',
             thumbnail: 'https://picsum.photos/id/1018/250/150/',
           },
           {
             original: 'https://picsum.photos/id/1015/1000/600/',
             thumbnail: 'https://picsum.photos/id/1015/250/150/',
           },
           {
             original: 'https://picsum.photos/id/1019/1000/600/',
             thumbnail: 'https://picsum.photos/id/1019/250/150/',
           },
         ]
      }

const Product = ({className}) =>(
    <div className={className}>
      <ImageGallery gallery={demoProduct.ImageGallery} />
      <ProductDetails product={demoProduct} />
    </div>
  )

const StyledProduct = styled(Product)`
  display:flex;
  flex-direction:row;
  justify-content:space-around;
  align-items:center;
  > * {
    width: 50%;
    padding: 1.5em;
  }
  @media(max-width: 768px) {
    flex-direction:column;
    > * {
      width: 100%;
    }
  }
`;

export default StyledProduct;
