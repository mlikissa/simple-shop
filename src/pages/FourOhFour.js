import React from 'react';


function FourOhFour() {
  return (
    <h1>Ooops, nothing here - sorry</h1>
  )
}
export default FourOhFour;
